import re

import xbmc
import xbmcgui
from os.path import join

path_join = join


def notify(header=None, msg='', duration=2000, sound=None, icon_path=''):
    try:
        xbmcgui.Dialog().notification(header, msg, icon_path, duration, sound)
    except:
        builtin = "XBMC.Notification(%s,%s, %s, %s)" % (header, msg, duration, icon_path)
        xbmc.executebuiltin(builtin)


def setQuery(url, key, val):
    if not url or not key or not val:
        return
    url = re.sub(r'' + re.escape(key) + '=[^&]', '', url)
    if re.search(r'\?', url):
        url = re.sub(r'&$', '', url) + '&' + key + '=' + str(val)
    else:
        url += '?' + key + '=' + str(val)
    return url
