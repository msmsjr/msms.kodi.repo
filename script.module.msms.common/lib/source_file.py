from xml.etree import ElementTree as ET

import xbmc
import xbmcvfs

sourceList = {'.fusion': 'http://fusion.tvaddons.ag',
              '.superrepo': 'http://srp.nu',
              '.msms': 'http://tv.msms.cf',
              '.mucky': 'http://muckys.mediaportal4kodi.ml',
              '.xunity': 'http://xunitytalk.me/xfinity',
              '.echocoder': 'http://echocoder.com/repo/',
              '.streamTeam': 'http://repo.streamteam.space',
              '.filmKodi': 'http://kodi.filmkodi.com'}


def indent(elem, level=0):
    i = "\n" + level * "  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level + 1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i


def _filterSrc(o, srcs):
    if srcs:
        return not len([src for src in srcs if src.find('./path').text.find(o[1]) > -1])
    return True


def updateSrc():
    src_path = xbmc.translatePath('special://home/userdata/sources.xml')
    if xbmcvfs.exists(src_path):
        try:
            tree = ET.parse(src_path)
            root = tree.getroot()
            fileTag = root.find('./files')
            fileSources = fileTag.findall('./source')
            filteredSrc = filter(lambda o: _filterSrc(o, fileSources), [(i, sourceList[i]) for i in sourceList])
            for source in filteredSrc:
                # xbmc.log('add source: %s-%s' % source)
                _createSource(fileTag, source[0], source[1])
            indent(root)
            tree.write('%s' % src_path, xml_declaration=True, encoding='utf-8', method="xml")
            return True
        except:
            pass
    return False


def _createSource(parent, key, val):
    src = ET.SubElement(parent, 'source')
    srcName = ET.SubElement(src, 'name')
    srcName.text = key
    srcPath = ET.SubElement(src, 'path')
    srcPath.text = val
    srcPath.set('pathversion', '1')
    srcShare = ET.SubElement(src, 'allowsharing')
    srcShare.text = 'true'
