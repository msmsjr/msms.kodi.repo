import re
import urllib2
from xml.etree import ElementTree as ET


def mapItem(i):
    o = {'thumbnail': ''}
    if i is not None:
        o['title'] = i.find('title').text
        o['link'] = i.find('enclosure').get('url')
        o['type'] = i.find('enclosure').get('type')
        desc = i.find('description')
        if desc:
            matchThumb = re.search('''<img[^>]*src=['"]([^'">]+)['"]''', desc.text)
            if matchThumb:
                o['thumbnail'] = matchThumb.group(1)
    return o


def getRss(url):
    tree = ET.parse(urllib2.urlopen(url))
    root = tree.getroot()
    channel = {}
    _channel = root.find('channel')
    if _channel is not None:
        channel['name'] = _channel.find('title').text
        channel['link'] = _channel.find('link').text
        channel['items'] = map(mapItem, _channel.findall('item'))
    return channel
