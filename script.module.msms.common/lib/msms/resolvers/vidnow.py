import re

from msms.common import get_content
from urlresolver.plugins.lib import helpers
from urlresolver.resolver import UrlResolver


def parse_sources_list(html):
    sources = []
    match = re.search('''['"]?sources['"]?\s*:\s*\[(.*?)\]''', html, re.DOTALL)
    if match:
        sources = [(match[1], match[0].replace('\/', '/')) for match in
                   re.findall('''['"]?file['"]?\s*:\s*(?:window\.atob\()['"]([^'"]+)['"][^}]*['"]?\)?label['"]?\s*:\s*['"]([^'"]*)''', match.group(1), re.DOTALL)]
    return [('%s' % label, link.decode('base64') if re.match('\w+=', link) else link) for label, link in sources]


class VidNowResolver(UrlResolver):
    name = 'VidNow'
    host = 'vidnow.to'
    domains = [host]
    pattern = '(?://|\.)(vidnow\.to)/(?:embed)/(.+)'

    def __init__(self):
        pass

    def get_url(self, host, media_id):
        # return 'http://%s/embed/%s' % (host, media_id)
        return self._default_get_url(host, media_id, template='http://{host}/embed/{media_id}')

    def get_media_url(self, host, media_id):
        html = get_content(self.get_url(host, media_id))
        sources = parse_sources_list(html)
        return helpers.pick_source(sources, auto_pick=False)

    @classmethod
    def _is_enabled(cls):
        return True
