import time
from threading import Thread

import xbmc

__author__ = 'msms'
__addonID__ = "msms.player"


class MyPlayer(xbmc.Player):
    def __init__(self):
        xbmc.Player.__init__(self)
        self.log = xbmc.log

    # def setPlaybackEndedCallback(self, playbackEndedCallback):
    #     # self.endCallback = playbackEndedCallback
    #     xbmc.log("{0!s}: {1!s}".format(__addonID__, "registered Playback_Ended callback"))

    def onPlayBackEnded(self):
        self.log("{0!s}: {1!s}".format(__addonID__, "Playback ended. notifying callback."))
        # self.endCallback()

    def onPlayBackStopped(self):
        self.log("{0!s}: {1!s}".format(__addonID__, "Playback stopped. notifying callback."))
        # self.endCallback()

    def onPlayBackStarted(self):
        self.log("{0!s}: {1!s}".format(__addonID__, "Playback started. notifying callback."))

    def onPlayBackPaused(self):
        self.log("{0!s}: {1!s}".format(__addonID__, "Playback paused. notifying callback."))

    def keepAlive(self):
        is_start = False
        while not xbmc.abortRequested:
            if self.isPlaying() and not is_start:
                # self.addon.sleep(2000)
                is_start, _start_time = True, time.time()
                self.log('[COLOR green]Start Playing..[/COLOR]')
            if is_start and not self.isPlaying():
                break
        self.log('Keep alive stopped..')
