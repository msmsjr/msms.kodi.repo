API_KEY = 'AIzaSyAmYHZ5YEuarlNzMWQ3C5rqVU5LwOhxlzM'


def generate_search_qs(term, match='term', videoDuration='any', after=None, search_music=False, key=API_KEY, order='relevance'):
    """ Return query string. """

    qs = {
        'q': term,
        'maxResults': 50,
        'safeSearch': "none",
        'order': order,
        'part': 'id,snippet',
        'type': 'video',
        'videoDuration': videoDuration,
        'key': key
    }

    # if after:
    #     after = after.lower()
    #     qs['publishedAfter'] = '%sZ' % (datetime.utcnow() - timedelta(days=DAYS[after])).isoformat() \
    #         if after in DAYS.keys() else '%s%s' % (after, 'T00:00:00Z' * (len(after) == 10))

    if match == 'related':
        qs['relatedToVideoId'] = term
        del qs['q']

    if search_music:
        qs['videoCategoryId'] = 10

    return qs
