import sys
import urllib2
from contextlib import contextmanager
from urllib import urlencode, unquote
from urllib2 import urlopen
from urlparse import parse_qsl

import xbmc
import xbmcaddon
import xbmcgui
import xbmcplugin
from . import cache
from urlresolver.hmf import HostedMediaFile

_plugin_url = sys.argv[0]
handle = int(sys.argv[1])
qs = sys.argv[2]
_addon = xbmcaddon.Addon()
_dialog = xbmcgui.Dialog()
kargs = dict((k, v) for k, v in parse_qsl(qs.lstrip('?')))

addon_name = _addon.getAddonInfo('name')
addon_id = _addon.getAddonInfo('id')
addon_version = _addon.getAddonInfo('version')
profile_dir = xbmc.translatePath(_addon.getAddonInfo('profile'))
get_string = _addon.getLocalizedString
root_path = 'special://home/addons/%s' % addon_id


def debug(s):
    xbmc.log(str(s), xbmc.LOGDEBUG)


def error(s):
    xbmc.log(str(s), xbmc.LOGERROR)


def action_url(action, **action_args):
    action_args['action'] = action
    for k, v in action_args.items():
        if type(v) is unicode:
            action_args[k] = v.encode('utf8')
    _qs = urlencode(action_args)
    return _plugin_url + '?' + _qs


def add_item(diritem):
    xbmcplugin.addDirectoryItem(**diritem)


def end_dir():
    xbmcplugin.endOfDirectory(handle)


def dir_item(label, url, image='', is_folder=True, context_menu=None):
    if not context_menu:
        context_menu = []
    listitem = xbmcgui.ListItem(label, iconImage=image, thumbnailImage=image)
    listitem.addContextMenuItems(context_menu, replaceItems=True)
    if not is_folder:
        listitem.setProperty('IsPlayable', 'true')
        listitem.setProperty("Video", "true")
    # this is unpackable for xbmcplugin.addDirectoryItem
    return dict(handle=handle, url=url, listitem=listitem, isFolder=is_folder)


def popup(s):
    try:
        # Gotham (13.0) and later
        _dialog.notification(addon_name, s)
    except AttributeError:
        _dialog.ok(addon_name, s)


def select(heading, options):
    return _dialog.select(heading, options)


def resolve(url):
    # import the resolvers so that urlresolvers pick them up
    hmf = HostedMediaFile(url)
    return hmf.resolve()


def sleep(ms):
    xbmc.sleep(ms)


def back_dir():
    # back one directory
    xbmc.executebuiltin('Action(ParentDir)')


def refresh():
    # refresh directory
    xbmc.executebuiltin('Container.Refresh')


def run_plugin(url):
    xbmc.executebuiltin(run_plugin_builtin_url(url))


def run_plugin_builtin_url(url):
    return 'RunPlugin(%s)' % url


def get_input(heading, keyword=''):
    xbmc.log('search: %s' % keyword)
    kb = xbmc.Keyboard(default=keyword, heading=heading)
    kb.doModal()
    if kb.isConfirmed():
        return kb.getText()
    return None


def get_addon_path():
    return xbmc.translatePath(_addon.getAddonInfo('path'))


def get_addon():
    return _addon


@contextmanager
def busy_indicator(timeout=2000):
    xbmc.executebuiltin('ActivateWindow(busydialog)')
    try:
        sleep(timeout)
        yield
    finally:
        xbmc.executebuiltin('Dialog.Close(busydialog)')


def set_resolved(url, title='', image=None, icon=None):
    li = xbmcgui.ListItem(title, path=url, thumbnailImage=image, iconImage=icon or image)
    li.setPath(url)
    # xbmc.log('handle: %s' % handle)
    xbmcplugin.setResolvedUrl(handle, True, li)


@cache.memoize()
def get_content(url):
    # only scrape within the site
    # if url.startswith(BASE_URL) or url.startswith(VPLAY_URL):  # good enough check
    try:
        # should not be larger than 1MB
        req = urllib2.Request(url)
        req.add_header('Referer', url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36')
        html = urlopen(req).read(1000 * 1000)
        return html
    except:
        raise
        # Exception('Bad URL: %s' % url)
