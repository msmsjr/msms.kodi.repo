import sys
from contextlib import contextmanager
from urllib import urlencode
from urlparse import parse_qsl

import xbmc
import xbmcgui
import xbmcplugin
from xbmcaddon import Addon as originalAddon


class Addon:
    execute = xbmc.executebuiltin

    def __init__(self, argv=sys.argv):
        self._addon = originalAddon()
        self.addon_name = self._addon.getAddonInfo('name')
        self.addon_id = self._addon.getAddonInfo('id')
        self.addon_version = self._addon.getAddonInfo('version')
        self.profile_dir = xbmc.translatePath(self._addon.getAddonInfo('profile'))
        self.root_path = 'special://home/addons/%s' % self.addon_id
        self._plugin_url = argv[0]
        self._handle = int(argv[1])
        self.qs = argv[2]
        self.kargs = dict((k, v) for k, v in parse_qsl(self.qs.lstrip('?')))
        self.get_setting = self._addon.getSetting
        self._dialog = None

    def set_setting(self, sid, value):
        if not isinstance(value, basestring): value = str(value)
        self._addon.setSetting(sid, value)

    def log(self, msg, level=xbmc.LOGNOTICE):
        xbmc.log('%s: %s' % (self.get_id(), str(msg)), level)

    def debug(self, s):
        self.log(s, xbmc.LOGDEBUG)

    def error(self, s):
        self.log(s, xbmc.LOGERROR)

    def start(self, actions):
        self.log(self.kargs)
        action_name = self.kargs.pop('action', 'index')  # popped
        if action_name in dir(actions) and callable(getattr(actions, action_name)):
            action_func = getattr(actions, action_name)
            action_func(**self.kargs)
        else:
            raise Exception('Invalid action: %s' % action_name)

    def action_url(self, action, **action_args):
        action_args['action'] = action
        for k, v in action_args.items():
            if type(v) is unicode:
                action_args[k] = v.encode('utf8')
        qs = urlencode(action_args)
        return self._plugin_url + '?' + qs

    def get_name(self):
        return self._addon.getAddonInfo('name')

    def get_id(self):
        return self._addon.getAddonInfo('id')

    def get_profile_dir(self):
        return xbmc.translatePath(self._addon.getAddonInfo('profile'))

    def get_addon_path(self):
        return xbmc.translatePath(self._addon.getAddonInfo('path'))

    @staticmethod
    def add_item(item):
        xbmcplugin.addDirectoryItem(**item)

    def dir_item(self, label, url, **arg):
        context_menu = arg.get('context_menu', None)
        image = arg.get('image', None)
        is_folder = arg.get('is_folder', True)
        path = arg.get('path', None)
        if context_menu is None: context_menu = []
        if not image: image = 'http://tv.msms.cf/icons/folder.png' if is_folder else 'http://tv.msms.cf/icons/video.png'
        listItem = xbmcgui.ListItem(label, iconImage=image, thumbnailImage=image, path=path)
        # listItem.setInfo('Video', {'plot': 'test plot', 'title': 'test title'})
        listItem.addContextMenuItems(context_menu, replaceItems=True)
        # listItem.setProperty('IsPlayable', 'false')
        # if not is_folder:
        #     # listItem.setProperty('IsPlayable', 'true')
        # listItem.setProperty("Video", "true")
        o = {'handle': self._handle, 'url': url, 'listitem': listItem, 'isFolder': is_folder}
        return o

    def end_dir(self, is_success=True):
        xbmcplugin.endOfDirectory(self._handle, is_success)

    def popup(self, s, heading=None, icon=None, time=2000):
        if not self._dialog: self._dialog = xbmcgui.Dialog()
        try:  # Gotham (13.0) and later
            self._dialog.notification(heading or self.get_name(), s, icon=icon or '%s/%s' % (self.root_path, 'icon.png'), time=time)
        except AttributeError:
            self._dialog.ok(heading or self.get_name(), s)

    def select(self, heading, options):
        return self._dialog.select(heading, options)

    @staticmethod
    def sleep(ms):
        xbmc.sleep(ms)

    def back_dir(self):
        # back one directory
        self.execute('Action(ParentDir)')

    def refresh(self):  # refresh directory
        self.execute('Container.Refresh')

    def run_plugin(self, url):
        self.execute(self.run_plugin_url(url))

    def update_container(self, url):
        self.execute('Container.Update(%s, False)' % url)

    @staticmethod
    def run_plugin_url(url):
        return 'RunPlugin(%s)' % url

    @staticmethod
    def get_input(heading, keyword=''):
        kb = xbmc.Keyboard(default=keyword, heading=heading)
        kb.doModal()
        if kb.isConfirmed():
            return kb.getText()
        return None

    @contextmanager
    def busy_indicator(self):
        xbmc.executebuiltin('ActivateWindow(busydialog)')
        try:
            yield
        finally:
            xbmc.executebuiltin('Dialog.Close(busydialog)')

    @staticmethod
    def append_to_playlist(url, **kwargs):
        playlist = xbmc.PlayList(xbmc.PLAYLIST_VIDEO)
        list_item = kwargs.get('list_item', xbmcgui.ListItem(kwargs.get('title', url)))
        if 'clear' in kwargs and kwargs['clear']: playlist.clear()
        act_list = {'image': list_item.setThumbnailImage}
        for key, value in kwargs.iteritems():
            if key in act_list:
                act_list[key](value)
        playlist.add(url, list_item, 1)
        xbmc.Player().playselected(1)
        return playlist

    def play(self, url, title='', image=None, icon=None, player=None):
        li = xbmcgui.ListItem(title, path=url if isinstance(url, basestring) else None, thumbnailImage=image or icon, iconImage=icon or image)
        if player:  player.play(url, li)
        xbmcplugin.setResolvedUrl(self._handle, True, li)
