import functools
import json
import re
import threading
from urlparse import urljoin

import urlresolver
import xbmc
from msms import cache, store
from resources.lib import cleanstring
from resources.lib.config import base_icon_url, folder_icon
from resources.lib.scrapper import malay, icdrama, myasian
from resources.lib.scrapper.malay import VPLAY_URL

# pafy.set_api_key(tube_utils.API_KEY)
from resources.lib.scrapper.myasian import get_category, resolve_link

URL_RE = 'msms\.(work|cf)'


def _action(func):
    def call(self, *args, **kwargs):
        self.addon.log('called: %s' % str((func.__name__, args, kwargs)))
        return func(self, *args, **kwargs)

    return call


def _dir_action(func):
    @_action  # Note: must keep this order to get func name
    @functools.wraps(func)
    def make_dir(self, *args, **kargs):
        dir_items = func(self, *args, **kargs)
        if not dir_items:
            return
        for di in dir_items:
            self.addon.add_item(di)
        if dir_items:
            self.addon.end_dir()

    return make_dir


def _get_icon(icon):
    return '%s/%s' % (base_icon_url, icon)


class Action:
    def __init__(self, addon):
        """

        :type addon: msms.addon.Addon
        """
        self.addon = addon
        self.dir_item = addon.dir_item
        self.action_url = addon.action_url

    def _make_cm(self, _title, act, **act_arg):
        if act == 'search': act_arg['title'] = _title
        _url = self.action_url(act, **act_arg)
        builtin_url = self.addon.run_plugin_url(_url)
        context_menu = (_title, builtin_url)
        return context_menu

    def _saved_to_list_cm(self, eng_name, is_folder, _url, image):
        return self._make_cm('Add to saved items', 'add_to_saved', eng_name=eng_name, is_folder=is_folder,
                             url=_url, image=image)

    @_dir_action
    def index(self):
        action_url, dir_item, make_cm = self.action_url, self.dir_item, self._make_cm
        return self._malay_menu() + [
            # dir_item('Live TV',
            #          action_url('live_tv'), image=_get_icon('vintage-tv.png')),
            # dir_item('Malay Zone', action_url('malay_menu'), image=_get_icon('discover_malaysia.png'),
            #          context_menu=[make_cm('Search Malay Content', 'search', search_type='malay'),
            #                        make_cm('Search Alternative', 'search', search_type='movie_drama')]),
            # dir_item('Asian Zone', action_url('asian_zone'), image=_get_icon('k-folder1.png'),
            #          context_menu=[make_cm('Search Asian Content', 'search', search_type='asian')]),
            # dir_item('Tube Collection', action_url('u_tube'), image=_get_icon('icon.png'),
            #          context_menu=[make_cm('Search Tube', 'search', search_type='tube')]),
            # dir_item('Movies and Drama', action_url('movie_drama'), image=_get_icon('a2.png')),
            dir_item('Search', action_url('search', search_type='malay'), image=_get_icon('search.png')),
            dir_item('Favorites', action_url('saved_list'), image=_get_icon('a-love.png'))]

    @_dir_action
    def asian_zone(self):
        action_url, dir_item, make_cm = self.action_url, self.dir_item, self._make_cm
        return [
            dir_item('Korean Drama', action_url('korean_shows', url=urljoin(icdrama.base_url, '/korean-drama/')),
                     context_menu=[make_cm('Search Asian Content', 'search', search_type='asian')],
                     image=_get_icon('k-folder1.png')),
            dir_item('Korean Drama 2', action_url('myasian', url=urljoin(myasian.BASE_URL, '/drama/')),
                     context_menu=[make_cm('Search Asian Content 2', 'search', search_type='myasian')],
                     image=_get_icon('k-folder1.png')),
            dir_item('Asian Movies', action_url('korean_shows', url=urljoin(icdrama.base_url, '/movies/')),
                     context_menu=[make_cm('Search Asian Content', 'search', search_type='asian')], image=_get_icon('movies.png')),
            dir_item('[COLOR cyan]Alternative 1[/COLOR]', action_url('myasian_cat'), image=_get_icon('a3.png'),
                     context_menu=[make_cm('Search Asian Content', 'search', search_type='myasian')]),
        ]

    @_dir_action
    def myasian_cat(self):
        return [self.dir_item(cat.get('title'), self.action_url('myasian', url=cat.get('url')), image=_get_icon('a1.png')) for cat in get_category()]

    @_dir_action
    def myasian(self, url=None, title=''):
        if not url: return None
        data, di = resolve_link(url), []
        _type, items = data.get('type'), data.get('items')

        def make_url(_url, **args):
            return self.addon.action_url('myasian', url=_url, **args)

        if _type == 'list':
            for item in items:
                _img, _title, is_folder = item.get('image'), item.get('title'), item.get('is_folder', True)
                _act_url = make_url(item.get('url'), title=_title)
                cm = self._saved_to_list_cm(_title, is_folder, _act_url, _img)
                di.append(self.dir_item(_title, _act_url, is_folder=is_folder, image=_img, context_menu=[cm]))
            if data.get('prev_url'): di.append(self.dir_item('Previous Page', make_url(data.get('prev_url')), image=_get_icon('prev2.png')))
            if data.get('next_url'): di.append(self.dir_item('Next Page', make_url(data.get('next_url')), image=_get_icon('next2.png')))
        if _type == 'source':
            if len(items) > 0:
                # self.addon.run_plugin(self.action_url('play_source', url=items[0]))
                self.play_source(items[0], title=title)
            else:
                self.addon.popup('Source not available, Please try again.')
        return di

    @_dir_action
    def live_tv(self):
        tvs = malay.get_tv_list()
        di = [self.dir_item(tv.get('title'), tv.get('link'), image=tv.get('thumbnail'), is_folder=False) for tv in tvs]
        if not di: self.addon.popup('No live tv streaming found')
        return di

    def _malay_menu(self):
        category = ['Filem', 'Telemovie', 'Drama', 'TvShow', 'Klasik', 'Indonesia', 'All']
        url = malay.V2_URL
        dli = []
        for item in category:
            itemUrl = '%s?' % url
            if item != 'All': itemUrl += 'category=%s' % item
            dli.append(self.dir_item('[COLOR {0}]{1}[/COLOR]'.format('blue' if item == 'All' else 'white', item),
                                     self.action_url('malay_list', url=itemUrl),
                                     context_menu=[self._make_cm('Search Malay Content', 'search', search_type='malay')],
                                     image=_get_icon('folder.png')))
        return dli

    @_dir_action
    def malay_menu(self):
        return self._malay_menu()

    _saved_list_key = 'saved_list'

    def _get_saved_list(self):
        try:
            return store.get(self._saved_list_key)
        except:
            pass
        try:  # backward compatible (try cache)
            return cache.get(self._saved_list_key)
        except KeyError:
            return []

    @_dir_action
    def saved_list(self):
        sl = self._get_saved_list()
        di_list = []
        # noinspection PyShadowingNames
        for eng_name, is_folder, url, image in sl:
            remove_save_url = self.action_url('remove_saved', eng_name=eng_name, is_folder=is_folder, url=url, image=image)
            builtin_url = self.addon.run_plugin_url(remove_save_url)
            cm = [('Remove item', builtin_url)]
            di_list.append(self.dir_item(eng_name, url, image=image, context_menu=cm, is_folder=bool(is_folder)))
        return di_list

    @_action
    def add_to_saved(self, eng_name, is_folder, url, image):
        with self.addon.busy_indicator():
            sl = self._get_saved_list()
            sl.insert(0, (eng_name, is_folder, url, image))
            uniq = set()
            sl = [x for x in sl if not (x in uniq or uniq.add(x))]
            store.put(self._saved_list_key, sl)
            self.addon.popup('Item add to favorites')

    @_action
    def remove_saved(self, eng_name, is_folder, url, image):
        sl = self._get_saved_list()
        sl.remove((eng_name, is_folder, url, image))
        store.put(self._saved_list_key, sl)
        self.addon.refresh()
        self.addon.popup('Item removed')

    @_dir_action
    def korean_shows(self, url, show_cantonese=False):
        di_list = []
        for eng_name, ori_name, show_url, image in [show for show in icdrama.shows(url)
                                                    if show_cantonese or not re.search('(?i)\(cantonese\)', show[0])]:
            _url = self.action_url('versions', url=show_url, title=eng_name, image=image)
            cm = self._saved_to_list_cm(eng_name, True, _url, image)
            di_list.append(self.dir_item(eng_name, _url, image=image, context_menu=[cm], is_folder=not re.search('/movies?/', url)))
        for page, page_url in icdrama.pages(url):
            _url = self.action_url('korean_shows', url=page_url)
            page_label = cleanstring.page(page)
            di_list.append(self.dir_item(page_label, _url))
        return di_list

    @_dir_action
    def versions(self, url, title='', image=None):
        _versions = [x for x in icdrama.versions(url) if not re.findall('Chinese Subtitles', x[0])]
        if len(_versions) == 1:
            ver, href = _versions[0]
            return self.episodes(href, title, image=image)
        else:
            di_list = []
            for label, version_url in _versions:
                url = self.action_url('episodes', url=version_url, title='%s %s' % (title, label), image=image)
                ver = cleanstring.version(label)
                di_list.append(self.dir_item(ver, url))
            return di_list

    @_dir_action
    def episodes(self, url, title='', image=None):
        episode_list = icdrama.episodes(url)
        if len(episode_list) > 0:
            di_list = []
            for _title, name, episode_url in episode_list:
                epi = cleanstring.episode(name)
                label = '%s | %s' % (_title or title, epi)
                act_url = self.action_url('mirrors', url=episode_url, title=label, image=image)
                di_list.append(self.dir_item(label, act_url, is_folder=False, image=image))
            return di_list
        else:
            return self.mirrors(url, title=title)

    @_dir_action
    def mirrors(self, url, title='', image=None):
        mirror_list = icdrama.mirrors(url)
        num_mirrors = len(mirror_list)
        if num_mirrors > 0:
            di_list = []
            for mirr_label, parts in mirror_list:
                for part_label, part_url in parts:
                    label = cleanstring.mirror(mirr_label, part_label)
                    act_url = self.action_url('play_source', url=part_url, title=label, image=image)
                    di_list.append(self.dir_item(label, act_url, is_folder=False, image=image))
            return di_list
        else:
            # if no mirror listing, try to resolve this page directly
            self.play_source(url, title=title, image=image)
            return []

    @_dir_action
    def malay_list(self, url='', page=1, image=None):
        dli = []
        data = malay.handleApi(url, page)
        self.addon.log((data, image))
        for item in data['data']:
            _img = item['thumbnail'] or image
            _action_url = self.action_url('malay_list_local', data=json.dumps(item).encode('utf-8'), image=_img)
            cm = self._saved_to_list_cm(item['title'], True, _action_url, _img)
            dli.append(self.dir_item(item['title'], _action_url, image=_img, context_menu=[cm]))
        if data['nextPage']:
            dli.append(self.dir_item('[COLOR {0}]{1}[/COLOR]'.format('blue', 'Next'),
                                     self.action_url('malay_list', url=url, page=data['nextPage']),
                                     # image=path_join(common.get_addon_path(), 'icons', 'video.png')
                                     # image=None
                                     ))
        if len(data['data']) == 1:
            item = data['data'][0]
            return self.malay_list_local(json.dumps(item).encode('utf-8'), image=item['thumbnail'] or image)
        return dli

    @_dir_action
    def malay_list_local(self, data, image=None):
        obj = json.loads(data)
        links = []
        if 'links' in obj:
            links = obj['links']
        title = obj['title']
        img = obj['thumbnail'] if 'thumbnail' in obj and obj['thumbnail'] else image
        if len(links) > 0 and re.search(URL_RE, links[0]['link']):
            if len(links) == 1 and 'link' in links[0]:
                return self.malay_list(links[0]['link'], image=img)
            return map(lambda x: self.dir_item(x['text'], self.action_url('malay_list', url=x['link'], image=img), image=img), links)
        elif len(links) > 0:
            def update_hostname(name):
                name = re.sub('(?i)biawak[^\s]+', 'meeya.net', name or '')
                name = re.sub('(?i)mengkarung[^\s]+', 'iman.com', name)
                return name.replace('www.', '')

            def mk_dir_item(link):
                act_url = self.action_url('play_source', url=link['link'], title=title, image=img)
                return self.dir_item('%s - %s' % (update_hostname(link['host']), title),
                                     act_url, image=img, is_folder=False,
                                     context_menu=[self._make_cm('Queue', 'append_queue', url=act_url, image=img, title=title)])

            return map(lambda l: mk_dir_item(l), links)

    @_action
    def append_queue(self, url, **kwargs):
        self.addon.append_to_playlist(url, **kwargs)

    @_dir_action
    def u_tube(self):
        dli = []
        # try:
        for line in malay.get_tube_list():
            dli.append(self.dir_item(line['title'], line['url'], is_folder=line['is_folder'], image=line['image'],
                                     context_menu=[self._make_cm('Search Tube', 'search', search_tube='tube')]))
        dli.append(self.dir_item('Search Tube', self.action_url('search', search_type='tube'), image=_get_icon('search.png')))
        # except:
        #     self.addon.popup('Error while processing data.')
        return dli

    # @_dir_action
    def search(self, title='Search', search_type=None, _q=None):
        if search_type:
            if not _q:
                _q = self.addon.get_input('[COLOR green]%s[/COLOR]' % title, keyword=store.get('search.key', ''))
            if _q:
                store.put('search.key', _q)
                # if search_type == 'tube':
                #     return self.addon.update_container("plugin://plugin.video.youtube/search/?q=%s" % _q)
                # elif search_type == 'asian':
                #     return self.addon.update_container(self.action_url('korean_shows', url=icdrama.search_url % _q))
                # elif search_type == 'myasian':
                #     return self.addon.update_container(self.action_url('myasian', url='%s/search/%s' % (myasian.BASE_URL, re.sub('\s+', '+', _q))))
                if search_type == 'malay':
                    return self.addon.update_container(self.action_url('malay_list', url='%s?q=%s' % (malay.V2_URL, _q)))
                elif search_type == 'movie_drama':
                    return self.addon.update_container(self.action_url('movie_drama', url='%s/search/%s' % (malay.VPLAY_URL, re.sub('\s+', '+', _q))))
        return False

    @_dir_action
    def movie_drama(self, is_cat=None, url=None):
        dir_item, action_url = self.dir_item, self.action_url
        if is_cat:
            return [self.dir_item(cat.get('text'), action_url('movie_drama', url=cat.get('url')), image=folder_icon) for cat in malay.vplay_get_category()]
        elif url:
            res = malay.vplay_get_list(url)
            is_folder = (res['type'] not in ['movie', 'episode']) if 'type' in res else True
            image = res['image'] if 'image' in res and res['image'] else None

            def makeItem(obj):
                _act_url = action_url('movie_drama', url=obj.get('url'))
                _img = obj['image'] if 'image' in obj else image
                cm = [self._saved_to_list_cm(obj.get('title'), is_folder, _act_url, _img)]
                return dir_item(
                    obj.get('title'),
                    _act_url
                    if is_folder else action_url('play_source', url=obj.get('url'), title=obj.get('title'), image=_img),
                    is_folder=is_folder, context_menu=cm,
                    image=_img)

            return [
                makeItem(o)
                for o in res['items']]
        return [
            dir_item('Movies', action_url('movie_drama', url='%s/movies' % VPLAY_URL), image=_get_icon('movies.png')),
            dir_item('TV Shows', action_url('movie_drama', url='%s/tvshows' % VPLAY_URL), image=_get_icon('k-folder1.png')),
            dir_item('Search', action_url('search', search_type='movie_drama'), image=_get_icon('search.png')),
            dir_item('Browse by category', action_url('movie_drama', is_cat=True), image=folder_icon),
        ]

    @_action
    def play_source(self, url, **kwargs):
        with self.addon.busy_indicator():
            _player = xbmc.Player()
            title = kwargs.get('title', '')
            image = kwargs.get('image', None)

            def monitor_play():
                is_start, _start_time = False, 0
                while not xbmc.abortRequested:
                    if _player.isPlaying() and not is_start:
                        break
                    else:
                        self.addon.sleep(1000)
                self.addon.sleep(3000)  # set timeout to indicate source playable or not
                if _player.isPlaying():
                    self.addon.popup('[COLOR blue]%s[/COLOR]' % title, heading='[COLOR green]Now Playing[/COLOR]')
                else:
                    self.addon.popup('[COLOR pink]Failed to play: %s[/COLOR]' % title, heading='[COLOR red]Failed to play[/COLOR]')

            try:
                if url:
                    resolved = None
                    if isinstance(url, basestring):
                        import msms.resolvers
                        resolved = urlresolver.resolve(url)
                    self.addon.log({'resolved': resolved, 'basestring': isinstance(url, basestring)})
                    self.addon.play(resolved or url, title=title, image=image, player=_player)
                    t = threading.Thread(target=monitor_play)
                    t.start()
                    t.join()
            except:
                self.addon.popup('[COLOR red]Sorry, Can\'t play from this source![/COLOR]')
            return url
