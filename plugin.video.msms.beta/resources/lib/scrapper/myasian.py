import re

from bs4 import BeautifulSoup

from msms import cache
from msms.common import get_content

BASE_URL = 'http://myasiantv.se'


@cache.memoize()
def _soup(url):
    soup = BeautifulSoup(get_content(url), 'html.parser')
    return soup


def get_category():
    return [{'title': 'Movies', 'url': '%s/movie' % BASE_URL}, {'title': 'Drama', 'url': '%s/drama' % BASE_URL}]


def resolve_link(url=''):
    is_movie_src = re.search("/movie/(?!page-\d+)\w+", url)
    if is_movie_src:
        url = '%s/watch' % re.sub('/watch.*', '', url.strip('/'))
    print url
    bs = _soup(url)
    items = []
    main_list = bs.select('.list .row div h2')
    episode_list = bs.select('ul.list-episode li h2')
    got_player = bs.select('.player iframe[src]')
    _type = 'list'
    next_btn, prev_btn = bs.find('a', class_='next'), bs.find('a', class_='prev')
    next_url, prev_url = None, None
    try:
        current_page = int(re.search('page-(\d+)', url).group(1))
    except:
        current_page = 1

    def is_list(_url):
        return not (re.search('/drama/[^/]+/(?!\?)[^/]+', _url) or re.search('/movie/(?!page-\d+)\w+', _url))

    if bs.find('ul', class_='pagination') and re.search('/(movie|drama)', url):
        if next_btn: next_url = '%s/page-%s' % (re.sub('/page-\d+', '', url.strip('/')), current_page + 1)
        if prev_btn: prev_url = '%s/page-%s' % (re.sub('/page-\d+', '', url.strip('/')), current_page - 1)
    if got_player or is_movie_src:
        for iframe in got_player:
            items.append((iframe.get('src')))
        _type = 'source'
    elif main_list:
        for _item in main_list:
            title = _item.getText().strip()
            parent = _item.find_parent('div')
            url = parent.find('a').get('href')
            img = parent.find('img').get('src')
            items.append({'title': title, 'url': url, 'image': img, 'is_folder': is_list(url)})
    elif episode_list:
        for _ep in episode_list:
            title = _ep.getText().strip()
            url = _ep.find('a').get('href')
            try:
                img = bs.find('img', class_='poster').get('src')
            except:
                img = ''
            items.append({'title': title, 'url': url, 'image': img, 'is_folder': is_list(url)})

    return {'type': _type, 'items': items, 'next_url': next_url, 'prev_url': prev_url}
