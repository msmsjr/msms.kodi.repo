import json

import xml2dict
from bs4 import BeautifulSoup
from msms.common import get_content
from msms_utils import setQuery

BASE_URL = 'http://api.msms.cf'
V2_URL = BASE_URL + '/malay'
VPLAY_URL = 'http://vplaytv.me'

import re
from urllib2 import urlopen
from msms import cache


@cache.memoize()
def _soup(url):
    soup = BeautifulSoup(get_content(url), 'html5lib')
    return soup


@cache.memoize(15)
def get_tv_list():
    try:
        return xml2dict.parse(get_content('%s/tv.xml' % BASE_URL))['items']['item']
    except:
        return []


@cache.memoize(15)
def handleApi(url, page=1):
    url = setQuery(url, 'page', 1 if not page else page)
    data = json.loads(re.sub('\\\\u[a-f0-9]{4}', '', get_content(url).encode('utf-8')).strip(), encoding='utf-8')
    data['nextPage'] = None
    if data and int(data.get('totalPage', '0')) > int(page):
        data['nextPage'] = int(page) + 1
    return data


@cache.memoize(360)
def vplay_get_category():
    bs = _soup('%s/blog' % VPLAY_URL)
    _res = [{'text': x.text, 'url': x.get('href')} for x in bs.find('ul', {'class': 'genres'}).find_all('a')]
    return _res


# @cache.memoize(60)
def vplay_get_list(url):
    data = get_content(url)
    items = []
    bs = BeautifulSoup(data, "html.parser")
    tv_shows = bs.select('body.single-tvshows ul.episodios li')  # list of episode
    # prev_page = bs.find('link', {'rel', 'prev'})
    # next_page = bs.find('link', {'rel', 'next'})
    # xbmc.log('hasNext: %s, hasPrev: %s' % (str(bool(prev_page)), str(bool(next_page))))
    if tv_shows:
        for li in tv_shows:
            img = li.find('img').get('src')
            [items.append({'image': img, 'url': a.get('href'), 'title': a.getText()}) for a in li.select('div.episodiotitle a[href]') if img]
        return {'type': 'tv-shows', 'items': items}

    body_genres = bs.find('body', {'class': 'tax-genres'})
    archive = bs.find('body', {'class': 'post-type-archive'})
    if body_genres or archive:
        for item in bs.select('.items article.item'):
            try:
                items.append({
                    'type': 'movie' if 'movies' in item.get('class') else 'tv-show',
                    'image': item.find('img').get('src'),
                    'title': item.find(class_='title').getText().strip(),
                    'description': item.find(class_='texto').getText().strip(),
                    'url': item.find('a').get('href')})
            except:
                continue
        return {'type': 'list', 'items': items}
    search_result = bs.select('.result-item')  # search result
    if search_result:
        for item in search_result:
            try:
                items.append({
                    'image': item.find('img').get('src'),
                    'title': re.sub('\s+', ' ', '%s' % item.find(class_='title').getText().strip()),
                    'description': re.sub("""\s+""", ' ', '%s' % item.find(class_='contenido').getText().strip()),
                    'url': item.find('a').get('href')})
            except:
                continue
        return {'type': 'list', 'items': items}

    body_movie = bs.find('body', {'class': 'single-movies'})  # single movie
    body_episode = bs.find('body', {'class': 'single-episodes'})  # single episode
    if body_movie or body_episode:
        sources = re.findall("""sources\s*:\s*\[({[^]]+)]""", data)
        title = bs.find('h1')
        title2 = ''
        description = ''
        title = title.string if title else ''
        _desc = bs.find('div', {'itemprop': 'description'})
        image = bs.find(class_='poster').find('img') if bs.find(class_='poster') else None
        image = image.get('src', '') if image else ''
        if _desc:
            title2 = _desc.find('h3').string if _desc.find('h3') else ''
            description = _desc.find('p').string if _desc.find('p') else ''
        if sources:
            items = map(lambda o: {'url': o.group(1), 'type': o.group(2), 'title': 'Play %s %s' % (title, o.group(3) or '')},
                        [re.match('file:"([^"]+)"(?:,\s*type:"([^"]+)")?(?:,\s*label:"([^"]+)")?', source.strip('{').strip('}')) for source in sources[0].split('},')])
        if not len(items):
            # common.popup('[COLOR red]No streaming [COLOR blue]source[/COLOR] available[/COLOR]')
            pass
        return {'type': 'episode' if body_episode else 'movie', 'items': items, 'description': description, 'title2': title2, 'title': title, 'image': image}

    return {'items': items}


@cache.memoize(10)
def get_tube_list():
    _list = []
    for line in urlopen('http://tv.msms.cf/tube.list'):
        _data = line.strip().strip('||').split('||')
        isFolder = True
        if _data:
            if len(_data) == 1:
                _data.append(_data[0])
            if len(_data) == 2:
                _data.append('')  # icon path
            originalLink = _data[1]
            matchPlay = re.findall('(playlist|watch).+(?:list|v)=([^&]+)', originalLink)
            matchUserChannel = re.findall('(channel|user)/([^/&]+)', originalLink)
            if matchPlay:
                if (matchPlay[0][0]).strip() == 'playlist':
                    cmd = "playlist/{0}/".format(matchPlay[0][1])
                else:
                    cmd = 'play/?%s=%s' % ('video_id', matchPlay[0][1])
                    isFolder = False
            elif matchUserChannel:
                cmd = '%s/%s/' % (matchUserChannel[0][0], matchUserChannel[0][1])
            else:
                continue
            pluginUrl = 'plugin://plugin.video.youtube/%s' % cmd
            _list.append({'url': pluginUrl, 'is_folder': isFolder, 'image': _data[2], 'title': _data[0]})
    return _list
