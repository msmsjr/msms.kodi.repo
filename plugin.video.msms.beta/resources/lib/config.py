import base64
from os.path import join as pathjoin

from msms import common
from msms.common import profile_dir

cache_file = pathjoin(profile_dir, 'cache.pickle')
store_file = pathjoin(profile_dir, 'store.pickle')
tracking_id = base64.b64decode('VUEtNzgwNTA2NTItMg==xx')
# base_icon_url = 'http://tv.msms.cf/icons'
base_icon_url = '%s/resources/icons' % common.root_path
folder_icon = '%s/folder.png' % base_icon_url
addon_icon = '%s/icon.png' % common.root_path


# context menu

def make_cm(title, act, **act_arg):
    if act == 'search':
        act_arg['title'] = title
    _url = common.action_url(act, **act_arg)
    builtin_url = common.run_plugin_builtin_url(_url)
    context_menu = [(title, builtin_url)]
    return context_menu
