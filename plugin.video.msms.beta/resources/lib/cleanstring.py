import re


def show(eng, orig):
    eng = eng.strip(' -')
    return eng


def page(_page):
    match = re.match(r'Page (\d+)$', _page)
    if match:
        _page = 'Page %s' % match.group(1)
    elif re.match(u'\u00ab First$', _page):
        _page = 'First Page'
    elif re.match(u'Last \u00bb$', _page):
        _page = 'Last Page'
    return '[I][ %s ][/I]' % _page


def version(_version):
    match = re.match(r'Watch online \(([^)]+)\)$', _version)
    if match: _version = match.group(1)
    return _version


def episode(_episode):
    match = re.match(r'(\d+)(?: \[END\]|)$', _episode)
    if match:
        return 'Episode %s' % match.group(1)
    elif re.match(r'\d{4}-\d{2}-\d{2}', _episode):
        return _episode
    return _episode


def mirror(_mirror, part):
    match = re.match(r'(?:Part ?|)(\d+)$', part)
    if match:
        part = 'Part %s' % match.group(1)
    return '[B]%s[/B] : %s' % (_mirror, part)
