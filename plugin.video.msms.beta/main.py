import sys
import traceback
from os.path import relpath, dirname

from msms.addon import Addon
from resources.lib.actions import Action

if __name__ == '__main__':
    try:
        addon = Addon()
        actions = Action(addon)
        addon.start(actions)
    except Exception as e:
        _, _, tb = sys.exc_info()
        here = dirname(__file__)
        for f, l, _, _ in traceback.extract_tb(tb):
            if f.startswith(here):
                filename = relpath(f, here)
                line = l
        # ga.exception('[%s] %s:%s' % (e, filename, line))
        raise
